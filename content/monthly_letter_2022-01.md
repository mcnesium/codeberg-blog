Title: Monthly Report December 2021 - Three years of Codeberg
Date: 2022-01-17
Category: Letters from Codeberg
Authors: Otto (Codeberg e. V.)

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

Dear Codeberg e.V. Members and Supporters!

We are starting a New Year, and wish you all the best for your goals and projects. If you can imagine to spend some time on improving Codeberg.org this year, please get in touch with us. We have plenty of diverse tasks and goals for the platform, including code work, community support and social media, infrastructure setups and much more. We are explicitly open to guiding newcomers or career changes and try to mentor as time permits.

We are hosting 20219 repositories, created and maintained by 17634 users. Compared to one month ago, this is an organic growth rate of +1635 repositories (+8.8% month-over-month) and +1135 users (+6.9%).


**Codeberg New Pages Launch**: On December 2nd, we finally launched the next version of the Codeberg Pages Server, bringing many new features like custom domains and serving from different pages and repos. The following weeks were spent with user support, bug fixing (ooops), refactoring and discussing the further roadmap of the service with your feedback. Don't have your very own Codeberg Page yet? Setting it up is as easy as it could be. Check out [Codeberg.Page](https://codeberg.page) or the [Social Media Launch Announcement](https://mastodon.technology/@codeberg/107378263569347099)!

**Public relations**: While we did not have resources to present a talk at rc3 this year, we ordered a batch of stickers and threw it into the stickers exchange. If you participated in it, you might have received some Codeberg stickers. If you can make use of a larger portion of Codeberg stickers for multiple hacker spaces and other nerdy hotspots, please send us an email and tell us where you are going to distribute them. We get back to you with an appropriately sized batch.


The machines are running at about 50% capacity (partly due to garbage collection and cleanup of quarantined repositories), as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 167 members in total, these are 120 members with active voting rights and 46 supporting members, and one honorary member.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.

