Title: Monthly Report July 2021 - 100+ active association members!
Date: 2021-08-18
Category: Letters from Codeberg
Authors: Codeberg e.V.

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*


Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!

We are hosting 14667 repositories, created and maintained by 12824 users. Compared to one month ago, this is an organic growth rate of +1335 repositories (+10% month-over-month) and +800 users (+6.7%).

**Performance**: Still remember the performance issues we mentioned two newsletters ago? While we are okay with the go-git backend for now, we also rolled out caching on production after a period of heavy testing on our staging servers. We're now sure that it will only have a positive impact on the site's performance and that the parameters used will not blow up RAM usage on prod. We're now seeing how much memory it will take to create a good cache for all your repos.

**CI**: Some team members are working on the CI service with Woodpecker and adding the missing bits and pieces to the software, which is a libre Drone fork with a lively and growing community. Woodpecker is not affiliated with any company, but indeed libre and only committed to its community.

**Search**: Searching repositories is awesome and we're having a running integration with our test server. It is deployed to codeberg-test and mostly working - but not for all repos yet. We're currently trying to identify an issue that is causing some repos not to be indexed yet. But you can already have a sneak preview at https://codeberg-test.org/ashimokawa/Gadgetbridge/search?q=device&t= or with your own repo uploaded there. If you would like to join our efforts to debug the open issues please let us know!

**Event**: We had the Codeberg Docuthon from July 10 to July 17 in order to improve the documentation and READMEs of your repos - because software documentation is a key to making your code reusable and allowing new contributors to join your project. Haven't participated or still work to do? Software documentation is always important, you can still explore repos with the Docuthon flag and write some words about your project. Our very first event was going fine, for our next we are looking forward to growing participation!

**Misc**: As always we did a lot of small tweaks on the fly. An interesting proof of concept is the migration of our build-deploy-gitea system from managing secrets (for MySQL and Redis database) via environment variables to generating them for every deployment. This way, we can avoid sharing these secrets between the admin team, and changing them in case of an issue is as easy as doing a new deployment. Also, a lot of work is being done to move server config and deployment to Ansible and further open it up to the community.


The machines are running at about 38% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 137 members in total, these are 101 members with active voting rights and 35 supporting members, and one honorary member. 

*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 
