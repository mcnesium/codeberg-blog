Title: Monthly Report October 2020
Date: 2020-11-11
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

We are hosting 6741 repositories, created and maintained by 5404 users. Compared to one month ago, this is an organic growth rate of +1324 repositories (+24.4% month-over-month) and +826 users (+18%).

The machines have been upgraded and are runnnig at about 65% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 87 members in total, these are 68 members with active voting rights and 19 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

