Title: Monthly Report January 2022 - Hardware maintenance is hard!
Date: 2022-02-16
Category: Letters from Codeberg
Authors: Otto (Codeberg e. V.)

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

Dear Codeberg e.V. Members and Supporters!

We are hosting 22020 repositories, created and maintained by 19055 users. Compared to one month ago, this is an organic growth rate of +2011 repositories (+10.1% month-over-month) and +1441 users (+8.2%).


**Gitea is joining the fediverse**: It's planned for a long time, and still not done, but we are really happy about all progress in this direction. Gitea now has some funding and work being started, and we are really excited to look forward to interconnect with other forges not only on the mission level in the future. If you are looking for a code adventure and interested in federation, make sure to check out the federation label in the gitea issue tracker, and consider a contribution!

**New Terms of Use**: In January, we finished our long-term project of rewriting the Terms of Use with a lot of user feedback - thank you for this! While it has been hard and time consuming to ask for, moderate and merge all different opinions, we are very happy that we have chosen this direction. Codeberg is a community effort, and we want all our members and users to participate. The voting ended early February with 46 Yes and 1 No vote.

**Hardware maintenance**: In late January, we detected critical read errors on our SSD after 9 months in production (and using 0.4% of the guaranteed writes), so we had to do urgent hardware maintenance on January 29th, resulting in an unexpected downtime for the services being hosted on that machine (mostly pages and CI, due to accumulation of other issues - Codeberg.org / Gitea was not affected). In addition to the replacement SSD we added a new SSD to the RAID. Before proceeding to move more services to own hardware, we are definitely going to focus on investing in improved redundancy as well as having more people available for maintenance work. Regular reminder that donations are pretty much appreciated.

The machines are running at about 53% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 172 members in total, these are 123 members with active voting rights and 48 supporting members, and one honorary member.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.

