Title: Monthly Report April 2020
Date: 2020-05-13
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

Codeberg e.V. has 63 members in total, these are 47 members with active voting rights and 16 supporting members. We are hosting 3012 repositories, created and maintained by 2544 users. Compared to one month ago, this is an organic growth rate of +442 repositories (+17.2% month-over-month) and +467 users (+22.5%).

The codeberg.org content is now loading **much** faster on mobile devices with severely restricted networking bandwith, especially when caching is disabled: In #176 user mappu04 reported that our setup was not yet doing on-the-fly content compression. After enabling this, we are now not looking too bad in the forgeperf.org benchmarks (see https://mastodon.technology/@codeberg/104133688512365495 for some background).

The machines are runnnig at about 57% capacity, as usual we will scale up as soon we approach the 66% threshold. In the coming weeks we plan to purchase hardware for an economic mid-end backup server doing automated offsite backups (job currently running on machine provided by founding members). If you would like to help in this project -- configuring/building/setting up the box -- please tell us!

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>
<https://codeberg.org><br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

