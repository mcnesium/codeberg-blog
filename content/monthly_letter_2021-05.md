Title: Monthly Report April 2021 - 10k users!
Date: 2021-05-05
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

We are hosting 11877 repositories, created and maintained by 10567 users (yeah, we hit the 10k!). Compared to one month ago, this is an organic growth rate of +1140 repositories (+10.6% month-over-month) and +1118 users (+11.8%).

The machines are running at about 35% capacity, as usual we will scale up once again we approach the 66% threshold.

Our new server has been ordered, expected delivery in coming weeks. These additional capacities will allow us to enable code search, and build new features.

Codeberg e.V. has 114 members in total, these are 85 members with active voting rights and 28 supporting members, and one honorary member.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 
