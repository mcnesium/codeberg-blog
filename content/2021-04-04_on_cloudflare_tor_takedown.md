Title: On the cloudflare-tor takedown
Date: 2021-04-04
Category: Announcement
Authors: Codeberg e.V.


In the last couple of days, we have received multiple inquiries to remove sensitive information from the crimeflare/cloudflare-tor repository and all clones and forks of that repository hosted on Codeberg.org.

This incident has some history: We have been made aware that this repository contains lists of usernames that are either linked with their Codeberg profile or their social media accounts and allegedly blamed as Cloudflare supporters without an evidence for their support. We started a discussion with the maintainers of this repository and asked to remove these sensitive information, that are apparently for shaming people (defamation), from their repo in a timely manner to comply with our Terms of Service as well as relevant German law. Note that emails and login aliases are also private data as of GDPR.

We apologize for any inconvenience caused to contributors and maintainers of this repository and we are looking forward to see you continue your work on free and open source content respecting relevant laws.

## Legal background
According to GDPR, we are obligued to remove sensitive user information as soon as a concerned person demands us to do so. This includes the immediate and complete removal of the data, its history and all copies (the forks of this repository in this case).

The content in question consists of multiple lists with sensitive information of users, that is obviously made for public shaming, as well as Cloudflare employee data, that are considered as private information and possibly constitutes incrimination of leaking private information in this particular context.

Codeberg e.V. has been founded in Germany and Codeberg.org is hosted in Germany, therefore we are tied to EU/German law.

## Our Position

We furthermore want to give a statement to the content of the repository itself:

People reaching out to us and to the maintainers of the repository itself tried to make clear that they do not consider themselves as Cloudflare-supporters, but critical opponents of this company, and thus could not even imagine a reason for being listed there.

While we do not want to advise on centralized network technology and do not rely on Cloudflare technology for the hosting of Codeberg for a good reason, we also think that using Cloudflare for a website does not make anyone directly a supporter, and even listing them as Cloudflare users (instead of supporters) does not change the fact that this list was obviously built for shaming and attacking those users for their choices and opinions. We can not accept anyone attacking and threatening us and our users (or anyone for that matter), or inciting others to do so. There is no freedom to do harm to others and we think the authors of this list were doing so by publishing this repository in its former form.
 
Codeberg is dedicated to supporting the development of Free and Open Source software, and we are hosting repositories that comply with our mission statement. The content in question is not dedicated to Open Source development, but rather shaming of a company and especially individual people. If someone wants to break monopoly of a big and proprietary software solution, we recommend everyone to start working on an alternative to their service like we at Codeberg have done for proprietary git hosting services. 

We did not use hate speech, we did not strive attacks against users of these platforms, yet we see people moving from their service to our open source solution day by day - and this makes us very proud. We are thankful to anyone who is joining and supporting our journey.

We do think that an open web can only be built by open and fair technologies and we want to support their development with our infrastructure. Supporting hate speech and attacks is not part of our mission and thus we request everyone to refrain from using Codeberg for such actions, forever.

Publicly posting criticism about worrisome tech companies and their products or business model (by providing reasonable arguments) is explicitly not a problem. We are always concerned about user privacy and encourage everyone to start working on viable alternatives to monopolistic data collection companies and/or spread information about their issues as well as share knowledge about existing alternatives.

